package com.noobchain.hyperledgerFabricJavaBackend;

import org.hyperledger.fabric.gateway.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class HyperledgerFabricJavaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(HyperledgerFabricJavaBackendApplication.class, args);

		// Load an existing wallet holding identities used to access the network.
		Path walletDirectory = Paths.get("wallet");
		Wallet wallet = Wallets.newFileSystemWallet(walletDirectory);

		// Path to a common connection profile describing the network.
		Path networkConfigFile = Paths.get("connection.json");

		// Configure the gateway connection used to access the network.
		Gateway.Builder builder = Gateway.createBuilder()
				.identity(wallet, "user1")
				.networkConfig(networkConfigFile);

		// Create a gateway connection
		try (Gateway gateway = builder.connect()) {

			// Obtain a smart contract deployed on the network.
			Network network = gateway.getNetwork("mychannel");
			Contract contract = network.getContract("fabcar");

			// Submit transactions that store state to the ledger.
			byte[] createCarResult = contract.createTransaction("createCar")
					.submit("CAR10", "VW", "Polo", "Grey", "Mary");
			System.out.println(new String(createCarResult, StandardCharsets.UTF_8));

			// Evaluate transactions that query state from the ledger.
			byte[] queryAllCarsResult = contract.evaluateTransaction("queryAllCars");
			System.out.println(new String(queryAllCarsResult, StandardCharsets.UTF_8));

		} catch (ContractException | TimeoutException | InterruptedException e) {
			e.printStackTrace();
		}
	}

}
